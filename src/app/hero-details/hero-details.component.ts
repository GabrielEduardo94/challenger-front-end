import { Hero } from './../hero';
import { Component, OnInit, Input } from '@angular/core';
import { HeroService } from '../hero.service';
import { HeroListComponent } from '../hero-list/hero-list.component';

@Component({
  selector: 'app-hero-details',
  templateUrl: './hero-details.component.html',
  styleUrls: ['./hero-details.component.css']
})
export class HeroDetailsComponent implements OnInit {

  @Input() hero: Hero;

  constructor(private heroService: HeroService, private listComponent: HeroListComponent) { }

  ngOnInit() {
  }

}
