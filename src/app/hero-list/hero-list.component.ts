import { Observable } from "rxjs";
import { HeroService } from "./../hero.service";
import { Hero } from "./../hero";
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hero-list',
  templateUrl: './hero-list.component.html',
  styleUrls: ['./hero-list.component.css']
})
export class HeroListComponent implements OnInit {

  hero: Observable<Hero[]>;

  constructor(private heroservice: HeroService) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData(){
    this.hero = this.heroservice.list();
  }

  updatehero(hero: Hero) {
    this.heroservice.updateHerois(hero.id,hero)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }

}
