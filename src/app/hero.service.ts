import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  private baseUrl = 'http://localhost:8080/herois';

  constructor(private http: HttpClient) { }

  herois(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  CreateHerois(hero: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, hero);
  }

  updateHerois(id: number, hero: Object): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, hero);
  }

  list(): Observable<any> {
    return this.http.get('http://localhost:8080/list');
  }
}
