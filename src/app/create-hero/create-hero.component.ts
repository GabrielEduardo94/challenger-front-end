import { HeroService } from './../hero.service';
import { Hero } from './../hero';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-create-hero',
  templateUrl: './create-hero.component.html',
  styleUrls: ['./create-hero.component.css']
})
export class CreateHeroComponent implements OnInit {

  hero: Hero = new Hero();
  submitted = false;

  constructor(private heroService: HeroService) { }

  ngOnInit() {
  }

  newHero(): void {
    this.submitted = false;
    this.hero = new Hero();
    this.hero.dataCadastro = String(Date.now);
  }

  save() {
    this.heroService.CreateHerois(this.hero)
      .subscribe(data => console.log(data), error => console.log(error));
    this.hero = new Hero();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

}
