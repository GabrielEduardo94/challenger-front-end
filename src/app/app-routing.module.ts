import { HeroDetailsComponent } from './hero-details/hero-details.component';
import { CreateHeroComponent } from './create-hero/create-hero.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeroListComponent } from './hero-list/hero-list.component';

const routes: Routes = [
  { path: '', redirectTo: 'hero', pathMatch: 'full' },
  { path: 'herois', component: HeroListComponent },
  { path: 'add', component: CreateHeroComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
