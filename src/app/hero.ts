export class Hero {
    id: number;
    nome: string;
    dataCadastro: string;
    Universo: Object;
    Poderes: Object;
    ativo: boolean;
}